<?php

namespace Soisy\Exceptions;

/**
 * Class InvalidRatingException
 *
 */
class InvalidRatingException extends \Exception
{ }
