<?php

namespace Soisy\Exceptions;

/**
 * Class InvalidAmountException
 *
 */
class InvalidAmountException extends \Exception
{ }
