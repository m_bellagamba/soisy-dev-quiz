<?php

namespace Soisy;

use Soisy\Exceptions\InvalidRatingException;
use Soisy\Exceptions\InvalidAmountException;

/**
 * Class Loan
 *
 * @package Soisy
 */
class Loan
{
    private $amount;
    private $rating;

    function __construct($amount, $rating)
    {
        if ($amount <= 0) throw new InvalidAmountException('Invalid amount');
        if ($rating < 1 || $rating > 5) throw new InvalidRatingException('Invalid rating');

        $this->amount = $amount;
        $this->rating = $rating;
    }

    function getAmount()
    {
        return $this->amount;
    }

    function getRating()
    {
        return $this->rating;
    }
}
