<?php

namespace Soisy;

/**
 * Class Matcher
 *
 * @package Soisy
 */
class Matcher
{
    private $loans = [];
    private $investments = [];
    private $matches = [];

    function addLoan(Loan $loan)
    {
        array_push($this->loans, $loan);
    }

    function getLoans()
    {
        return $this->loans;
    }

    function addInvestment(Investment $investment)
    {
        array_push($this->investments, $investment);
    }

    function getInvestments()
    {
        return $this->investments;
    }

    function addMatch(Loan $loan, Investment $investment)
    {
        array_push($this->matches, [
            'loan' => $loan,
            'investment' => $investment,
        ]);
    }

    function getMatches()
    {
        return $this->matches;
    }

    function run()
    {
        foreach ($this->loans as $loan) {
            foreach ($this->investments as $investment) {
                if ($this->isAMatch($loan, $investment)) {
                    $this->addMatch($loan, $investment);
                }
            }
        }
    }

    private function isAMatch(Loan $loan, Investment $investment)
    {
        return $loan->getRating() == $investment->getRating()
            && $loan->getAmount() <= $investment->getAmount();
    }
}
